# Geektrust problems

Source code for all geektrust problems.

## Set 5

`tame_of_thrones.py` is the solution to https://www.geektrust.in/coding-problem/backend/tame-of-thrones.
Run as:

    ./tame_of_thrones.py 1 # for problem 1
    ./tame_of_thrones.py 2 # for problem 2

