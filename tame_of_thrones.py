#!/usr/bin/python2

import random
import sys

# Constants
# Map of kingdoms and their emblems
KINGDOMS = { 
    'Space' : 'Gorilla', 
    'Land' : 'Panda', 
    'Water' : 'Octopus', 
    'Ice' : 'Mammoth', 
    'Air' : 'Owl', 
    'Fire' : 'Dragon' 
}
# List of messages that can be sent
MESSAGES = [
    "Summer is coming",
    "a1d22n333a4444p",
    "oaaawaala",
    "zmzmzmzaztzozh",
    "Go risk it all",
    "Let's swing the sword together",
    "Die or play the tame of thrones",
    "Ahoy! Fight for me with men and money",
    "Drag on Martin!",
    "When you play the tame of thrones, you win or you die.",
    "What could we say to the Lord of Death? Game on?",
    "Turn us away and we will burn you first",
    "Death is so terribly final while life is full of possibilities.",
    "You Win or You Die",
    "His watch is Ended",
    "Sphinx of black quartz judge my dozen vows",
    "Fear cuts deeper than swords My Lord.",
    "Different roads sometimes lead to the same castle.",
    "A DRAGON IS NOT A SLAVE.",
    "Do not waste paper",
    "Go ring all the bells",
    "Crazy Fredrick bought many very exquisite pearl emerald and diamond jewels.",
    "The quick brown fox jumps over a lazy dog multiple times.",
    "We promptly judged antique ivory buckles for the next prize.",
    "Walar Morghulis: All men must die."
]

def problemOne(rulerName, rulerKingdom):
    """ 
    Read input messages in the form 'Kingdom, message'
    Iterate on all inputs and find which ones contain letters of emblem
    Output list of allies based on matches
    """
    rulerOfSoutheros = 'None'
    alliesOfRuler = 'None'
    allies = []
    writeOutput(rulerOfSoutheros, alliesOfRuler)
    userInputs = []
    print "Input Messages to Kingdoms from King Shan (kingdom, message):"
    while True:
        rawInput = raw_input()
        if rawInput:
            userInputs.append(rawInput)
        else:
            break
    for userInput in userInputs:
        inputArray = userInput.split(',')
        kingdom = inputArray[0].strip()
        message = inputArray[1].strip()
        if kingdom in KINGDOMS and kingdom.upper() != rulerKingdom.upper():
            if findWordInString(KINGDOMS[kingdom].upper(), message.upper()):
                allies.append(kingdom)
        else:
            print "Warning: Bad kingdom name " + kingdom
    if (len(allies) >= 3):        
        rulerOfSoutheros = rulerName
        alliesOfRuler = ', '.join(allies)
        writeOutput(rulerOfSoutheros, alliesOfRuler)
    else:
        print "Less than 3 allies"

def problemTwo(countOfRandomMessages):
    """
    Read input for kingdom names competing
    Generate random messages to other kingdoms from each competitor
    Count ballot based on emblem matching
    Output ballot results
    """
    userInputs = []    
    messageFromCompetingRulers = []
    rulerOfSoutheros = 'None'
    alliesOfRuler = 'None'
    writeOutput(rulerOfSoutheros, alliesOfRuler)
    print "Enter the Kingdoms competing to be the ruler (space separated):"
    rawInput = raw_input()
    if rawInput:
        inputKingdoms = rawInput.split(' ')
        if len(inputKingdoms) == 1:
            print "Error: Need more than 1 kingdom as input"
            return
        randomMessages = pickRandomMessages(countOfRandomMessages, inputKingdoms)
        if len(randomMessages) == 0:
            print "Error: Cannot generate random messages"
            return
        (allianceCount, allianceGiven) = findRulerProblemTwo(inputKingdoms, randomMessages, countOfRandomMessages)
        sortedListOfAlliances = sorted(allianceCount.values(), reverse=True)
        rulerOfSoutheros = ''.join([k for k,v in allianceCount.items() if v == sortedListOfAlliances[0]])
        alliesOfRuler = ' '.join([k for k,v in allianceGiven.items() if v > 0])
        writeOutput(rulerOfSoutheros, alliesOfRuler)     

def writeOutput(rulerOfSoutheros, alliesOfRuler):
    """ Printing function """
    print "Who is the ruler of Southeros?"
    print rulerOfSoutheros
    print "Allies of Ruler?"
    print alliesOfRuler

def findWordInString(needle, haystack):
    """
    Return true if letters in needle are found in same number in haystack
    """
    count = 0
    for n in needle:
        if (needle.count(n) <= haystack.count(n)):
            count += 1
    if (count == len(needle)):
        return True
    else:
        return False

def writeResultsProblemTwo(roundBallot, allianceCount):
    """ Printing function """
    print "Results after round %s ballot count" % roundBallot
    for kdom, allianceRecieved in allianceCount.iteritems():
        print "Allies for %s : %d" % (kdom, allianceRecieved) 
        
def ballotProcessProblemTwo(inputKingdoms, randomMessages):
    """
    Count ballots based on emblem word matching per kingdom
    """
    allianceCount = {}
    allianceGiven = {}
    for msg in randomMessages:
        msgArray = msg.split(',')
        sender = msgArray[0].strip()
        receiver = msgArray[1].strip()
        if sender not in allianceCount:
            allianceCount[sender] = 0
        if receiver not in allianceGiven:
            allianceGiven[receiver] = 0
        messageRecieved = msgArray[2].strip()
        if receiver not in inputKingdoms and allianceGiven[receiver] == 0:
            if findWordInString(KINGDOMS[receiver].upper(), messageRecieved.upper()):
                allianceGiven[receiver] += 1
                allianceCount[sender] += 1
    return (allianceCount, allianceGiven)

def findRulerProblemTwo(inputKingdoms, randomMessages, countOfRandomMessages):
    """
    Function called for problem 2 to repeat ballot counting until winner is found
    """
    roundBallot = 1
    (allianceCount, allianceGiven) = ballotProcessProblemTwo(inputKingdoms, randomMessages)
    while True:
        writeResultsProblemTwo(roundBallot, allianceCount)
        sortedListOfAlliances = sorted(allianceCount.values(), reverse=True)
        if (sortedListOfAlliances[0] == sortedListOfAlliances[1]):
            tiedKingdoms = [k for k,v in allianceCount.items() if v == sortedListOfAlliances[0]]
            roundBallot += 1
            randomMessages = pickRandomMessages(countOfRandomMessages, tiedKingdoms)
            (allianceCount, allianceGiven) = ballotProcessProblemTwo(inputKingdoms, randomMessages)
        else:
            break
    return (allianceCount, allianceGiven)

def pickRandomMessages(countOfRandomMessages, inputKingdoms):
    """
    Pick random messages to be sent to kingdoms
    """
    randomMessages = []
    messageFromCompetingRulers = []
    for sender in inputKingdoms:
        if sender in KINGDOMS:
            for eachKingdom in KINGDOMS:
                if (sender != eachKingdom):
                    receiver = eachKingdom
                    messageToReceiver = random.choice(MESSAGES)
                    messageFromCompetingRulers.append(sender + ', ' + receiver + ', ' + messageToReceiver)
        else:
            print "Warning: Kingdom " + sender + " not found"
    if (len(messageFromCompetingRulers) > countOfRandomMessages):
        randomMessages = random.sample(messageFromCompetingRulers, countOfRandomMessages)
        return randomMessages
    else:
        print "Cannot pick %d random message from total messages of %d" %(countOfRandomMessages, len(messageFromCompetingRulers))
        return []

def usage():
    print "Usage: " + sys.argv[0] + " 1/2 : 1->problem1 2->problem2"

if __name__ == '__main__':
    if len(sys.argv) != 2:
        usage()
        sys.exit(1)
    else:
        if sys.argv[1] == '1':
            rulerName = 'King Shan'
            rulerKingdom = 'Space'
            problemOne(rulerName, rulerKingdom)
        elif sys.argv[1] == '2':
            countOfRandomMessages = 6
            problemTwo(countOfRandomMessages)
        else:
            usage()
            sys.exit(1)
